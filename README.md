# OpenNeuro Collector

Tool to get [OpenNeuro](https://openneuro.org/) datasets for curation and indexing.

OpenNeuro uses Git Annex which allows us to simply clone the repositories and the large files are not fetched.

Additionally, we use `sparse-checkout` to get only the files we're interested in.

## Usage

```
python3 collect.py --help
```

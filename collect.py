import argparse
import sys
import os
import requests
import json

# Return list of dataset repositories as teruned by GitHub API.
# r['name'] is repository name
# r['clone_url'] is the URL to clone
def get_all_data_repos():
    url = "https://api.github.com/orgs/OpenNeuroDatasets/repos?type=all&per_page=100&page=1"
    res = requests.get(url)
    repos=res.json()
    while 'next' in res.links.keys():
        res=requests.get(res.links['next']['url'])
        repos.extend(res.json())
    return repos

# Return a list of datasets info.
# Each dataset must have 'name' and 'clone_url'.
def get_repos_from_list(file_path):
    git_clone_url_prefix = "https://github.com/OpenNeuroDatasets/"
    repos = []
    with open(file_path, mode='r') as datasets_file:
        for line in datasets_file:
            name = line.strip()
            if len(name) > 0:
                repos.append({'name' : name, 'clone_url' : git_clone_url_prefix + name + '.git'})
    return repos

# Command line interface
def main(argv):
    parser = argparse.ArgumentParser(
        description = "Collects datasets from OpenNeuro by cloning the \
                       respective data repositories and using sparse-checkout.")
    parser.add_argument(
        '-l', '--list',
        dest = "datasets_file_path",
        metavar = "DATASETS",
        help = "Input file with datasets (one id per line)."
    )
    parser.add_argument(
        '--verbose', '-v',
        dest = "verbose",
        action = "store_true",
        help = "If set, the output is more verbose."
    )
    args = parser.parse_args()
    
    repos = []
    if args.datasets_file_path:
        if args.verbose:
            print('INFO Collecting repositories URLs')
        repos = get_repos_from_list(args.datasets_file_path)
    else:
        repos = get_all_data_repos()
    
    if args.verbose:
        print('INFO There are', len(repos), 'datasets to clone')
    
    # Load file patters for sparse-checkout
    patterns = []
    with open('patterns.json', mode='r') as patterns_file:
        patters = json.load(patterns_file)

    # Put datasets in a separate directory.
    datasets_dir = 'datasets'
    if not os.path.exists(datasets_dir):
        os.mkdir(datasets_dir)
    os.chdir(datasets_dir)
    
    # Clone repos sparsely.
    # Get only the files specified in `patterns.json`.
    for r in repos:
        if args.verbose:
            print('\tINFO Cloning data repository', r['name'])
        # Clone sparsely
        os.system("git clone --filter=blob:none --sparse --depth 1 --quiet " + r['clone_url'])
        os.chdir(r['name'])
        # Set to no-cone mode (allows all patterns)
        os.system("git config core.sparseCheckoutCone false")
        # Empty the sparse-checkout rules (by default top-level files are fetched)
        os.system("> .git/info/sparse-checkout")
        # Add all our rules
        for p in patters:
            os.system("git sparse-checkout add " + p)
        os.chdir('..')

if __name__ == '__main__':
    main(sys.argv)
